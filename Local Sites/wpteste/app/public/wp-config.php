<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CKdWI+5NY0gnj2m5vMnvnuC+WtaItIXBAS6Sj+yXGsAp3Ocszi5crXrO3tT9Q5uJgHya/TjSBCwFw7COEIsj6g==');
define('SECURE_AUTH_KEY',  'p5pateDMAf0DSkPKJSoChDUckF1EsL9lppakjR6toEptAQdYHTdCfHM5Hxici++29Heoik8K8D5oiC20UPKifw==');
define('LOGGED_IN_KEY',    'gwxzNgpCn2Jey/AoAX6NkebKBOuaUnQo2Rc2oW7AGa29IRGfsd0bkzo3x15tpWy6rjINgSGss65h063dIM8iqw==');
define('NONCE_KEY',        'Cr/XTUi8lxYSlh4iZCZVNTmxIGjmVQjAwA7XRX3m+Aqyk+y/TuBgWplGFZnY+pa0kfULyV32jcwh9PTn40KHrw==');
define('AUTH_SALT',        'UkNMhy13dwnSayFoobaGlU2SCbBWYhQXOMkckpHHZUTvDxsDbIu28Q4+1HD+FB0yvad0sIARLZZJS5odxClEBQ==');
define('SECURE_AUTH_SALT', 'J5gyuLpiSNFZe7QcwngfCyigkQvrM1BONu/+gzVjmvm25MOsE7QDACVzKGHd6pTU6Dvll809yuV8Rxh1smdV4A==');
define('LOGGED_IN_SALT',   'tYftag8Zb2/937cpXeqYue8aPdq0PeokvpDUdefsblRS9BA7tDgwz2sqa/hpIuo9CIkz8kkiV0WzadePS9ASjA==');
define('NONCE_SALT',       'xMAPTnlAJiKhXBramx+j22f5/UqIA9svPcja8JvBZYMVfLuA3mCq2DoNRXvFiJu03xldvKTGUaVKrFtqgYuMOA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
